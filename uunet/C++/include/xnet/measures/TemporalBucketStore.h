#ifndef UU_XNET_MEASURES_TEMPORALBUCKET_H_
#define UU_XNET_MEASURES_TEMPORALBUCKET_H_

#include "xnet/datastructures/graphs/TemporalTextNetwork.h"

namespace uu {
namespace net {



template<typename T>
struct TempMessage{
  std::shared_ptr<T> p;
  uu::core::Time time;
};

template<typename T>    
class TemporalBucket
{
 public:
  //Variables
  TempMessage<T>* t;
  uu::core::Time min_time;
  uu::core::Time interval;
  std::map<uu::core::Time, unsigned int> referrers;


  //Helper functions
  std::shared_ptr<T>
  get_vertex();

  
  std::pair<const uu::core::Time, unsigned int> 
  get_bucket_peak(
  );

  
  uu::core::Time
  get_persistence(
     float difference
  );
  
  std::map<uu::core::Time, float> 
  get_bulk_bucket_velocity(
  );

  float
  get_bucket_velocity(
      unsigned int from_production
  );

  unsigned int
  get_duration(
  );
  
  uu::core::Time
  get_time();

  void
  synch_referrers(
	std::map<uu::core::Time, unsigned int>& synched_ref
  );


};

 
/**
 * A TemporalBucketStore is a container for partitions of a time interval in a graph.
 */

template <typename T>
class TemporalBucketStore 
{
 public:
  uu::core::Time earliest_production;
  std::map<T, TemporalBucket<T>> store;

  TemporalBucket<T>
  get_bucket(
     T v
  );

  void
  synchronize(
  );

  void
  trim_time(
       uu::core::Time end_point 
  );

  std::map<T, std::map<uu::core::Time, float> > 
  tbs_moving_average(
	   uu::core::Time t_interval,
	   unsigned int window,
	   unsigned int synch
  );
};

//TemporalBucket


template <typename T>
inline const typename T::key_type& last_key(const T& pMap)
{
    return pMap.rbegin()->first;
}

//Templataize
template <typename T>
std::pair<const uu::core::Time, unsigned int> 
TemporalBucket<T>::
get_bucket_peak()
{

 auto peak = max_element(referrers.begin(),
			  referrers.end(),
			  [](const std::pair<uu::core::Time, unsigned int>& b1,
			     const std::pair<uu::core::Time, unsigned int>& b2) {
        return b1.second < b2.second; });

  return *peak;
}
 
template <typename T>
unsigned int
TemporalBucket<T>::
get_duration(
)
{
  unsigned int duration;
  auto start = referrers.begin()->first;
  auto end   = referrers.rbegin()->first;
  auto diff  = uu::core::time_sub(end, start);
  std::cout << "Duration: " << uu::core::to_string(end) << " - " << uu::core::to_string(start) << " = " << uu::core::time_to_epoch(diff) << std::endl; 
  duration = uu::core::time_to_epoch(diff);
  return duration;
}

//Get the velocity at each time step based on the response numbers
template <typename T>
std::map<uu::core::Time, float> 
TemporalBucket<T>::
get_bulk_bucket_velocity()
{
  std::map<uu::core::Time, float>  v;
  
  auto curr_time = get_time();
  auto time = uu::core::time_to_epoch(interval);

  if(referrers.empty())
    return v;
  
  auto last_partition = referrers.rbegin()->first;
  
  while(uu::core::time_leq(curr_time, last_partition))
    {
      float initial = referrers.count(curr_time) * referrers.find(curr_time)->second;
      float final   = referrers.count(uu::core::time_add(curr_time, interval)) * referrers.find(uu::core::time_add(curr_time, interval))->second;
      auto curr_v  = (float)(final - initial)/time;      
      
      v.insert(std::pair<uu::core::Time, float>(curr_time, curr_v));
      curr_time = uu::core::time_add(curr_time, interval);
      
    }
  
  return v;
}
 
//Get the max velocity, either from time of production or first response
//Susceptible to sporadic singleton answer before the mass responses begin
template <typename T>
float
TemporalBucket<T>::
get_bucket_velocity(
     unsigned int from_production)
{

  uu::core::Time start_time;

  //Time starts are offset by the interval size to incorporate every response
  //Otherwise it risks jumping over a response if it shares the same interval
  //as the start or first response
  
  //Set the time start to the point of message production
  if(from_production)
    start_time =  uu::core::time_sub(get_time(), interval);

  //Set time time start to the point just before the first response
  else
    start_time = uu::core::time_sub(referrers.begin()->first, interval);

  //Get time til peak;
  auto peak = get_bucket_peak();

  auto time_diff = uu::core::time_to_epoch(uu::core::time_sub(peak.first, start_time));

  if(referrers.empty() || time_diff == 0)
    return 0;

  auto v  = (float)peak.second/time_diff;      
  
  return v;
}
 

 
template<typename T>
uu::core::Time
TemporalBucket<T>::
get_time(
        )
{
  return t->time;
}


template<typename T>
std::shared_ptr<T>
TemporalBucket<T>::
get_vertex(
        )
{
  return t->p;
}
 
template <typename T>
void
TemporalBucket<T>::
synch_referrers(
		std::map<uu::core::Time, unsigned int>& synched_ref
		)	
{
  referrers.swap(synched_ref);
}

//TemporalBucketStore

template <typename T>
TemporalBucket<T>
TemporalBucketStore<T>::
get_bucket(
     T v
)
{
  return store.at(v);
}


//Synchronizes messages by simply pushing messages back to the earlist message
//By the difference between them.
template <typename T>
void
TemporalBucketStore<T>::
synchronize() 
{

  //Get buckets from TemporalBucket Store store
  for(auto &it: store)
    {
      auto &curr_b = it.second;
      auto t_diff = uu::core::time_sub(curr_b.t->time, earliest_production);
      curr_b.t->time = earliest_production; //Check to see whether it changes or not


      std::map<uu::core::Time, unsigned int> synch_ref;

      //Get referrers from current bucket referrer
      for(auto &p: curr_b.referrers)
	{

	  synch_ref.insert(std::pair<uu::core::Time, unsigned int>(uu::core::time_sub(p.first, t_diff), p.second));
	  }
      curr_b.synch_referrers(synch_ref);  
    }
 }

//Trims the interval size. end_point repressents the time for the last partition
//Everything after this is erased, useful for visualization
template <typename T>
void
TemporalBucketStore<T>::
trim_time(
     uu::core::Time end_point
) 
{
  for(auto &it: store)
    {
      auto &ref = it.second.referrers;
      
      for(auto &p: ref)
	{
	  if(uu::core::time_leq(p.first, end_point))
	     continue;
	  else
	    {
	      auto erase_index = ref.find(p.first);
	      ref.erase(erase_index, ref.end());
	      break;
	    }
	}
    }
}




template <typename T>
std::map<T, std::map<uu::core::Time, float> > 
TemporalBucketStore<T>::
tbs_moving_average(
	   uu::core::Time t_interval,
	   unsigned int window,
	   unsigned int synch
)
{


  std::map<T, std::map<uu::core::Time, float> > moving_averages;
  
  if(synch)
    synchronize();

  //Access the bucket store
  for(auto b: store)
    {
      if(b.second.referrers.size() == 0)
	continue;
	    
      std::map<uu::core::Time, float> curr_avg;
      curr_avg = moving_avg(b.second, window);
      moving_averages.insert(std::pair<T, std::map<uu::core::Time, float> >(b.first, curr_avg));
    }
  
  return moving_averages;
}
 
 
}
}
#endif
