#ifndef UU_XNET_MEASURES_MOVING_AVG_H_
#define UU_XNET_MEASURES_MOVING_AVG_H_

#include "xnet/measures/TemporalBucketStore.h"

namespace uu {
namespace net {

    
//TODO
//Check whether window size is odd or even, affects the results and may require further smoothing
template <typename T>
std::map<uu::core::Time, float>
inline moving_avg(
	   TemporalBucket<T> bucket,
	   unsigned int window
)
{

  std::map<uu::core::Time, float> m_avg;

  auto time_window = uu::core::time_mult(bucket.interval, window);
  auto starting_offset = uu::core::time_sub(bucket.get_time(), time_window); //Start offset to initalize graph with zero    
  auto curr_time = starting_offset;
  auto last_partition = uu::core::time_add(bucket.referrers.rbegin()->first, time_window);
  auto start_window = uu::core::time_add(curr_time, time_window);

  unsigned int central_tp = (window/2);   //temporary, Used to place moving average in most accurate time point
  float window_sum = 0;
  float window_avg = 0;

  //Calculate first window
  for (curr_time;
       curr_time < start_window;
       curr_time = uu::core::time_add(curr_time, bucket.interval))
    {
      window_sum = window_sum + (bucket.referrers.count(curr_time) * bucket.referrers.find(curr_time)->second);  
    }

  window_avg = window_sum/(float)window;
  //Remove entries where the moving average is zero, minimizes data sizes
  //The time variable maintains the relevant information
  if(window_avg != 0)
    {
      auto final_time = uu::core::time_sub(curr_time, uu::core::time_div(bucket.interval, central_tp));
      m_avg.insert(std::pair<uu::core::Time, float>(final_time, window_avg));
    }
  
  for (curr_time;
       curr_time < last_partition;
       curr_time = uu::core::time_add(curr_time, bucket.interval))
    {
      auto time_out = uu::core::time_sub(curr_time, time_window);
      float out = bucket.referrers.count(time_out)  * bucket.referrers.find(time_out)->second;
      float in  = bucket.referrers.count(curr_time) * bucket.referrers.find(curr_time)->second;
      window_sum = window_sum + (in - out);
      window_avg = window_sum/(float)window;

      if(window_avg == 0)
	{
	  continue;
	}

      auto final_time = uu::core::time_add(curr_time, uu::core::time_mult(bucket.interval, central_tp));
      m_avg.insert(std::pair<uu::core::Time, float>(final_time, window_avg));
    }

  
  return m_avg;
}



//templatize
std::pair<uu::core::Time, float>
inline get_ma_peak(
       std::map<uu::core::Time, float> ma
)
{
  auto peak = max_element(ma.begin(),
			  ma.end(),
			  [](const std::pair<uu::core::Time, float>& p1,
			     const std::pair<uu::core::Time, float>& p2) {
        return p1.second < p2.second; });
  
  return *peak;
}

 
float
inline get_ma_sum(
	std::map<uu::core::Time, float> ma 
)
{
  float init = 0;
  const float sum = std::accumulate(begin(ma), end(ma), init,
	[](const float previous,
	   const auto& element)
	{return previous + element.second; });
  return sum;
					     
}

//Get the amount of elements not equal to zero
float
inline get_ma_size(
	std::map<uu::core::Time, float> ma 
)
{
  float size = 0;
  for(auto &v: ma)
    {
      if(v.second > 0)
	size++;
    }
  return size;
}

//NOT ACCEPTABLE!
//FIND OUT WHY ERAS NEED TO BE CALLED TWICE TO DELETE EVERY 0-VALUED OBJECT!
std::map<uu::core::Time, float>
inline filter_ma_zeroes(
	std::map<uu::core::Time, float> ma 
)
{
  auto filtered = ma;
  
  for(auto &v: filtered)
    {
      if(v.second == 0)
	filtered.erase(v.first);
    }

    for(auto &v: filtered)
    {
      if(v.second == 0)
	filtered.erase(v.first);
    }
  
  return filtered;
}
 
std::pair<uu::core::Time, float>
inline get_ma_max_velocity(
       std::map<uu::core::Time, float> v
)
{
  auto max = max_element(v.begin(),
			  v.end(),
			  [](const std::pair<uu::core::Time, float>& p1,
			     const std::pair<uu::core::Time, float>& p2) {
        return p1.second < p2.second; });
  
  return *max;
}

std::pair<uu::core::Time, float>
inline get_ma_min_velocity(
       std::map<uu::core::Time, float> v
)
{
  auto min = max_element(v.begin(),
			  v.end(),
			  [](const std::pair<uu::core::Time, float>& p1,
			     const std::pair<uu::core::Time, float>& p2) {
        return p1.second > p2.second; });
  
  return *min;
}
 
//Also used to get acceleration
std::map<uu::core::Time, float> 
inline get_ma_velocity(
	std::map<uu::core::Time, float> ma,
	uu::core::Time interval
)
{
  std::map<uu::core::Time, float>  v;
  
  auto curr_time = ma.cbegin()->first;
  auto time = uu::core::time_to_epoch(interval); // Get time for division

  if(ma.empty())
    return v;

  auto last_partition = last_key(ma);
  
  while(uu::core::time_leq(curr_time, last_partition))
    {
      float initial = ma.count(curr_time) * ma.find(curr_time)->second;
      float final   = ma.count(uu::core::time_add(curr_time, interval)) * ma.find(uu::core::time_add(curr_time, interval))->second;
      auto curr_v  = (float)(final - initial)/time;
      
      v.insert(std::pair<uu::core::Time, float>(curr_time, curr_v));
      curr_time = uu::core::time_add(curr_time, interval);      
    }

  return v;
}


 

uu::core::Time
inline get_ma_persistence(
		std::map<uu::core::Time, float> ma,
		float difference
)
{

  auto peak = get_ma_peak(ma);
  auto target = peak.second * (1 - difference);
  auto l_bound = ma.lower_bound(peak.first);
  auto u_bound = ma.upper_bound(peak.first);
  auto hill = ma.begin()->first;
  auto valley = last_key(ma);

  
  for (std::map<uu::core::Time, float>::iterator it = l_bound; it != ma.begin(); --it)
    {
      if(it->second <= target)
	{
	  hill = it->first;
	   break;
	}
    }

    for (std::map<uu::core::Time, float>::iterator it = u_bound; it != ma.end(); ++it)
    {
      if(it->second <= target)
	{
	  valley = it->first;
	  break;
	}
    }

  return uu::core::time_sub(valley, hill);

 }


float
inline get_ma_standard_deviation(
		std::map<uu::core::Time, float> ma
)
{
  auto sum = get_ma_sum(ma);
  auto size = get_ma_size(ma);
  auto mean = sum/size ;
  auto filtered_ma = filter_ma_zeroes(ma);
  float sigma = 0;

  
  for(auto &m: filtered_ma)
    {
      sigma += pow((m.second - mean), 2);
    }
  
  auto variance = sigma / size;
  auto sd = sqrt(variance);

  return sd;

 }

uu::core::Time
inline get_ma_sd_persistence(
		std::map<uu::core::Time, float> ma
)
{

  auto peak = get_ma_peak(ma);
  auto target = peak.second - get_ma_standard_deviation(ma);
  auto l_bound = ma.lower_bound(peak.first);
  auto u_bound = ma.upper_bound(peak.first);
  auto hill = ma.begin()->first;
  auto valley = last_key(ma);

  
  for (std::map<uu::core::Time, float>::iterator it = l_bound; it != ma.begin(); --it)
    {
      if(it->second <= target)
	{
	  hill = it->first;
	   break;
	}
    }

    for (std::map<uu::core::Time, float>::iterator it = u_bound; it != ma.end(); ++it)
    {
      if(it->second <= target)
	{
	  valley = it->first;
	  break;
	}
    }

  return uu::core::time_sub(valley, hill);

 }

 
}
}
#endif
