#ifndef UU_NET_MEASURES_VERTEX_IMPORTANCE_H_
#define UU_NET_MEASURES_VERTEX_IMPORTANCE_H_

#include "xnet/measures/degree.h"


namespace uu {
namespace net {


  /*
template<typename G>
double
message_importance(
    const G* g,
    const Vertex* v 
);
  */


//Returns the nimportance of input vertex
template<typename G>
double
message_importance(
    const G* g,
    const Vertex* v
)
{
  core::assert_not_null(g, "degree", "g");
  core::assert_not_null(g, "degree", "v");


  double in_degree = uu::net::degree(g, v, uu::net::EdgeMode::IN);
  double total_out = uu::net::total_out_degree(g);

  return (in_degree/total_out);
}

//Returns the highest importance
template<typename G>
double
maximum_importance(
    const G* g
)
{
  core::assert_not_null(g, "degree", "g");
  
  
  double max = 0;
  double curr;
  for(auto v: *g->messages()->vertices())
    {
      curr = message_importance(g, v);
      if(curr > max)
	max = curr;
    }

  return max;
}




//Returns the actor of the most important message
template<typename G>
const Vertex*
key_message(
    const G* g
)
{
  core::assert_not_null(g, "degree", "g");
  
  double max = 0;
  double curr;
  const Vertex* key;
  
  for(auto v: *g->messages()->vertices())
    {
      curr = message_importance(g, v);
      if(curr > max)
	{
	max = curr;
	key = v;
	}
    }
  
  return key;
 }

 
//Returns the actor of the most important message
//Assumes an interlayer_edge input
//Assumes the only IN-degree of the message is the author
template<typename G>
const Vertex*
key_actor(
    const G* g
)
{  
  core::assert_not_null(g, "degree", "g");

  auto key  = key_message(g);
  auto author_list = *g->interlayer_edges()->incident(key, uu::net::EdgeMode::IN);
  auto author = author_list.get_at_index(0)->v1;
  
  return author;
 }

 
//TODO summary()

 
}
}

#endif
