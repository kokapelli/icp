/**
 * History:
 * - 2018.03.09 file created, following a restructuring of the previous library.
 */

#ifndef UU_XNET_DATASTRUCTURE_GRAPHS_TEMPORALTEXTNETWORK_H_
#define UU_XNET_DATASTRUCTURE_GRAPHS_TEMPORALTEXTNETWORK_H_

#include <memory>
#include <string>
#include "net/datastructures/graphs/AttributedSimpleGraph.h"
#include "xnet/measures/TemporalBucketStore.h"
#include "xnet/datastructures/graphs/TextGraph.h"
#include "xnet/measures/moving_avg.h"
#include "mnet/datastructures/graphs/MultilayerNetwork.h"
#include "mnet/datastructures/stores/UnionVertexStore.h"
#include "mnet/datastructures/stores/VertexDisjointLayerStore.h"
#include "mnet/datastructures/stores/TemporalInterlayerEdgeStore.h"
#include "core/attributes/Time.h"
//#include "mnet/datastructures/store_types.h"

namespace uu {
namespace net {

class
    TemporalTextNetwork
    :
    public MultilayerNetwork<
    UnionVertexStore,
    VertexDisjointLayerStore<TextGraph, AttributedSimpleGraph>,
    TemporalInterlayerEdgeStore<2>
  >
  
{

    typedef MultilayerNetwork<
    UnionVertexStore,
    VertexDisjointLayerStore<TextGraph, AttributedSimpleGraph>,
    TemporalInterlayerEdgeStore<2>
    > super;

  public:

    //using super::super;

    TemporalTextNetwork(
        const std::string& name,
        MultilayerNetworkType t,
        std::unique_ptr<UnionVertexStore> v,
        std::unique_ptr<VertexDisjointLayerStore<TextGraph, AttributedSimpleGraph>> l,
        std::unique_ptr<TemporalInterlayerEdgeStore<2>> e
    );

    TextGraph*
    messages(
    );

    const TextGraph*
    messages(
    ) const;

    AttributedSimpleGraph*
    actors(
    );

    const AttributedSimpleGraph*
    actors(
    ) const;

    TemporalSimpleEdgeStore*
    interlayer_edges(
    );

    const TemporalSimpleEdgeStore*
    interlayer_edges(
    ) const;

    const Edge*
    get_msg_sender(
       const Vertex* message
    ) const;

    const uu::core::Time
    get_sender_time(
	 const Edge* edge
    ) const;

    unsigned int
    get_time_diff(const Vertex* message_a,
		  const Vertex* message_b
    );
  
    const uu::core::Time
    get_latest_response(
	  const Vertex* message
    ) const;

    
    void
    randomize_intralayer_IN(
    );

    std::string
    summary(
    ) const;

    template <typename T>
    std::unique_ptr<TemporalBucketStore<T>>
    create_tbs(
	       uu::core::Time t_interval
     );
    
    template <typename T>
    std::unique_ptr<TemporalBucketStore<T>>
    create_grouped_tbs(
		       uu::core::Time t_interval,
		       unsigned int levels
     );

    std::vector<const Vertex*>
    group(
	  const Vertex* message,
	  unsigned int levels
     );
	

    template <typename T>
    std::map<T, std::map<uu::core::Time, float> > 
    moving_average(
	       uu::core::Time t_interval,
	       unsigned int,
	       unsigned int synch
     );
};


template <typename T>
std::unique_ptr<TemporalBucketStore<T>>
TemporalTextNetwork::
create_tbs(
	   uu::core::Time t_interval
)
{

  std::unique_ptr<uu::net::TemporalBucketStore<T>> tbs = std::make_unique<TemporalBucketStore<T>>();
  std::map<T, TemporalBucket<T>> tbs_store;
  
  //Why? [0], requires a begin, cannot find it otherwise?
  for(auto message: messages()->vertices()[0])
    {
      auto actor = get_msg_sender(message);
      auto msg_time = get_sender_time(actor);
      auto neighbors = messages()->edges()->neighbors(message, uu::net::EdgeMode::IN);
      auto latest_response = get_latest_response(message);

      uu::net::TemporalBucket<T> *tb = new TemporalBucket<T>();
      uu::net::TempMessage<T>    *tm = new TempMessage<T>();
      std::shared_ptr<T> t_msg = std::make_shared<T>(message);

      tm->p = t_msg;
      tm->time = msg_time;

      tb->t = tm;
      //Needed at all?
      tb->min_time = interlayer_edges()->attr()->get_min_time().value;
      tb->interval = t_interval;

      
      //Set referrers
      for(auto n: neighbors[0])
	{
	  auto curr_time = msg_time;
	  auto n_actor = get_msg_sender(n);
	  auto n_msg_time = get_sender_time(n_actor);
	  
	  if(n_msg_time < tb->min_time)
	    tb->min_time = n_msg_time;

	  //Add messages to correct buckets
	  //Assumes that no response comes before the production of current message
	  while(true)
	    {
	      if(uu::core::time_add(tb->interval, curr_time) >= n_msg_time)
		{
		  auto match = tb->referrers[curr_time]++;
		  break;
		}
	      curr_time = uu::core::time_add(tb->interval, curr_time);
	    }
	}
      tbs_store.insert( std::pair<T, TemporalBucket<T>>(message, *tb) );
    }
  tbs->earliest_production = interlayer_edges()->attr()->get_min_time().value;
  tbs->store = tbs_store;
  return tbs;
}


//TODO
template <typename T>
std::unique_ptr<TemporalBucketStore<T>>
TemporalTextNetwork::
create_grouped_tbs(
	uu::core::Time t_interval,
	unsigned int levels
)
{

  std::unique_ptr<uu::net::TemporalBucketStore<T>> tbs = std::make_unique<TemporalBucketStore<T>>();
  std::map<T, TemporalBucket<T>> tbs_store;
  
  //Why? [0], requires a begin, cannot find it otherwise?
  for(auto message: messages()->vertices()[0])
    {
      auto actor = get_msg_sender(message);
      auto msg_time = get_sender_time(actor);

      uu::net::TemporalBucket<T> *tb = new TemporalBucket<T>();
      uu::net::TempMessage<T>    *tm = new TempMessage<T>();
      std::shared_ptr<T> t_msg = std::make_shared<T>(message);
      //Templatize
      //std::vector<const Vertex*> grouped_messages;

      tm->p = t_msg;
      tm->time = msg_time;

      tb->t = tm;
      //Needed at all?
      tb->min_time = interlayer_edges()->attr()->get_min_time().value;
      tb->interval = t_interval;

      auto grouped_messages = group(message, levels);      //Set referrers
      
      for(auto n: grouped_messages)
	{
	  auto curr_time = msg_time;
	  auto n_actor = get_msg_sender(n);
	  auto n_msg_time = get_sender_time(n_actor);
	  
	  if(n_msg_time < tb->min_time)
	    tb->min_time = n_msg_time;

	  //Add messages to correct buckets
	  //Assumes that no response comes before the production of current message
	  while(true)
	    {
	      if(uu::core::time_add(tb->interval, curr_time) >= n_msg_time)
		{
		  auto match = tb->referrers[curr_time]++;
		  break;
		}
	      curr_time = uu::core::time_add(tb->interval, curr_time);
	    }
	}
      tbs_store.insert( std::pair<T, TemporalBucket<T>>(message, *tb) );
    }
  tbs->earliest_production = interlayer_edges()->attr()->get_min_time().value;
  tbs->store = tbs_store;
  return tbs;
}

 
template <typename T>
std::map<T, std::map<uu::core::Time, float> > 
TemporalTextNetwork::
moving_average(
	   uu::core::Time t_interval,
	   unsigned int window,
	   unsigned int synch
)
{


  std::map<T, std::map<uu::core::Time, float> > moving_averages;
  
  auto tbs = create_tbs<T>(t_interval);

  
  if(synch)
    tbs->synchronize();
  //Access the bucket store
  for(auto b: tbs->store)
    {
      if(b.second.referrers.size() == 0)
	continue;
      
      std::map<uu::core::Time, float> curr_avg;
      curr_avg = moving_avg(b.second, window);
      moving_averages.insert(std::pair<T, std::map<uu::core::Time, float> >(b.first, curr_avg));
    }
  
  return moving_averages;
}
 
/**
 * Creates a multiplex network.
 */
std::unique_ptr<TemporalTextNetwork>
create_temporal_text_network(
    const std::string& name
);

 
}
}

#endif
