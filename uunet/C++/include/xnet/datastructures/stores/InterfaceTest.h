#ifndef UU_XNET_DATASTRUCTURES_STORES_INTERFACETEST_H_
#define UU_XNET_DATASTRUCTURES_STORES_INTERFACETEST_H_


#include "mnet/datastructures/stores/TemporalInterlayerEdgeStore.h"
#include "net/datastructures/stores/SimpleAttributeStore.h"

namespace uu {
namespace net {
  
//std::vector< std::pair<int, uu::Time> >


  
class TemporalAttributes
{
 public:
  virtual
  const EdgeList*
  get_edges(
    
      ) = 0;
};


class Test : public TemporalAttributes
{
 public:
  get_edges(){};
};
 
}
}

#endif
