/**
 * History:
 * - 2018.03.09 file created, following a restructuring of the previous library.
 */

#ifndef UU_NET_DATASTRUCTURES_OBJECTS_EDGEDIR_H_
#define UU_NET_DATASTRUCTURES_OBJECTS_EDGEDIR_H_

namespace uu {
namespace net {

/** Directionality of edges. */
enum class EdgeDir
{
    UNDIRECTED,
    DIRECTED
};


}
}

#endif
