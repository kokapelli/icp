/**
 * Defines an object identifier type, to be used to identify all objects in the library.
 *
 * History:
 * - 2018.01.01 file adapted from version 1.0 of the multinet library
 */

#ifndef UU_CORE_DATASTRUCTURES_OBJECTS_OBJECTID_H_
#define UU_CORE_DATASTRUCTURES_OBJECTS_OBJECTID_H_

#include <cstdint>

namespace uu {
namespace core {

/* Identifiers */
using ObjectId = std::uint32_t;

}
}

#endif
