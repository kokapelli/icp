file(REMOVE_RECURSE
  "CMakeFiles/net-tests.dir/datastructures/connectivity/Walk_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/connectivity/Path_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/graphs/AttributedEmptyGraph_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/graphs/SimpleGraph_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/graphs/AttributedSimpleGraph_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/graphs/MultiGraph_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/AttributedMultiEdgeStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/AttributedVertexStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/VertexStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/AttributeStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/AttributedSimpleEdgeStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/MultiEdgeStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/datastructures/stores/SimpleEdgeStore_test.cpp.o"
  "CMakeFiles/net-tests.dir/algorithms/components_test.cpp.o"
  "CMakeFiles/net-tests.dir/algorithms/BFS_test.cpp.o"
  "CMakeFiles/net-tests.dir/algorithms/DFS_test.cpp.o"
  "CMakeFiles/net-tests.dir/algorithms/sssp_test.cpp.o"
  "CMakeFiles/net-tests.dir/community/VertexCommunity_test.cpp.o"
  "CMakeFiles/net-tests.dir/main_test.cpp.o"
  "CMakeFiles/net-tests.dir/io/read_attributed_simple_graph_test.cpp.o"
  "CMakeFiles/net-tests.dir/io/read_common_test.cpp.o"
  "CMakeFiles/net-tests.dir/io/read_simple_graph_test.cpp.o"
  "CMakeFiles/net-tests.dir/io/read_multi_graph_test.cpp.o"
  "CMakeFiles/net-tests.dir/measures/degree_test.cpp.o"
  "../../net-tests.pdb"
  "../../net-tests"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/net-tests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
