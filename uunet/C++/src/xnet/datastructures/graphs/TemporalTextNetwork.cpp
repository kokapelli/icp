/**
 * History:
 * - 2018.03.09 file created, following a restructuring of the previous library.
 */
#include "xnet/datastructures/graphs/TemporalTextNetwork.h"
#include "net/datastructures/objects/EdgeDir.h"
#include <iterator>

namespace uu {
namespace net {


TemporalTextNetwork::
TemporalTextNetwork(
    const std::string& name,
    MultilayerNetworkType t,
    std::unique_ptr<UnionVertexStore> v,
    std::unique_ptr<VertexDisjointLayerStore<TextGraph, AttributedSimpleGraph>> l,
    std::unique_ptr<TemporalInterlayerEdgeStore<2>> e
) :
  super(name, t, std::move(v), std::move(l), std::move(e))
{
}

TextGraph*
TemporalTextNetwork::
messages(
)
{
    return layers()->template get<0>();
}


const TextGraph*
TemporalTextNetwork::
messages(
) const
{
    return layers()->template get<0>();
}

AttributedSimpleGraph*
TemporalTextNetwork::
actors(
)
{
    return layers()->template get<1>();
}

const AttributedSimpleGraph*
TemporalTextNetwork::
actors(
) const
{
    return layers()->template get<1>();
}

TemporalSimpleEdgeStore*
TemporalTextNetwork::
interlayer_edges(
)
{
    return super::interlayer_edges()->get<0,1>();
}

const TemporalSimpleEdgeStore*
TemporalTextNetwork::
interlayer_edges() const
{
    return super::interlayer_edges()->get<0,1>();
}




  
const uu::net::Edge*
TemporalTextNetwork::
get_msg_sender(
      const uu::net::Vertex* message
) const
{
  auto actor = interlayer_edges()->incident(message, uu::net::EdgeMode::IN)->get_at_index(0);

  return actor;
}

const uu::core::Time
TemporalTextNetwork::
get_sender_time(
     const uu::net::Edge* edge
) const
{
  auto message_time = interlayer_edges()->attr()->get_time(edge).value;
  return message_time;
}

unsigned int
TemporalTextNetwork::
get_time_diff(const Vertex* message_a,
	      const Vertex* message_b
	      )
{
  auto actor_a = get_msg_sender(message_a);
  auto message_time_a = get_sender_time(actor_a);
  
  auto actor_b = get_msg_sender(message_b);
  auto message_time_b = get_sender_time(actor_b);

  unsigned int diff = uu::core::time_to_epoch(message_time_b) - uu::core::time_to_epoch(message_time_a);
  return diff;
}

  
const uu::core::Time
TemporalTextNetwork::
get_latest_response(
     const Vertex* message
) const
{
  auto actor = get_msg_sender(message);
  auto message_time = get_sender_time(actor);
  auto neighbors = messages()->edges()->neighbors(message, uu::net::EdgeMode::IN);
  //std::cout << "N: " << neighbors->size() <<std::endl;
  uu::core::Time latest_response = message_time;
  
  for(unsigned int i = 0; i < neighbors->size(); i++)
    {
      auto curr_actor = get_msg_sender(neighbors->get_at_index(i));
      auto curr_message_time = get_sender_time(curr_actor);
      if(curr_message_time >= latest_response)
	latest_response = curr_message_time;
    }

  return latest_response;
}

std::string
TemporalTextNetwork::
summary(
) const
{
    size_t num_edges = interlayer_edges()->size();

    size_t num_msg = messages()->vertices()->size();
    size_t num_act = actors()->vertices()->size();


    std::string summary =
        "TemporalTextNetwork (" +
        std::to_string(num_act) + (num_act==1?" actor, ":" actors, ") +
        std::to_string(num_msg) + (num_msg==1?" message, ":" messages, ") +
        std::to_string(num_edges) + (num_edges==1?" edge)":" edges)");
    return summary;
}
		   

std::unique_ptr<TemporalTextNetwork>
create_temporal_text_network(
    const std::string& name
)
{
    auto vs = std::make_unique<UnionVertexStore>();

    using LS = VertexDisjointLayerStore<TextGraph, AttributedSimpleGraph>;

    std::unique_ptr<TextGraph> g1 = create_text_graph("messages", (EdgeDir::DIRECTED));
    std::unique_ptr<AttributedSimpleGraph> g2 = create_attributed_simple_graph("actors");
    auto ls = std::make_unique<LS>(std::move(g1), std::move(g2));

    auto es = std::make_unique<TemporalInterlayerEdgeStore<2>>(EdgeDir::DIRECTED);

    std::unique_ptr<TemporalTextNetwork> net;
      
    MultilayerNetworkType t;
    net = std::make_unique<TemporalTextNetwork>(
              name,
              t,
              std::move(vs),
              std::move(ls),
              std::move(es));

    std::unique_ptr<UnionObserver<const Vertex, UnionVertexStore>> o1 = std::make_unique<UnionObserver<const Vertex, UnionVertexStore>>(net->vertices());
    net->messages()->vertices()->attach(o1.get());
    net->actors()->vertices()->attach(o1.get());
    net->register_observer(std::unique_ptr<core::GenericObserver>(std::move(o1)));

    return net;

}


std::vector<const Vertex*>
get_selection(
	      std::vector<std::pair<uu::core::Time, const Vertex*>> sorted,
	      uu::core::Time bound
	      )
{
  std::vector<const Vertex*> selection;


  for (auto iter = sorted.rbegin(); iter != sorted.rend(); ++iter) {
    if(uu::core::time_lt(bound, iter->first))
      selection.push_back(iter->second);
    else
      break;
       
  }
  return selection;
}
  
void
TemporalTextNetwork::
randomize_intralayer_IN(
)
{
  //Sort messages according to time
  //Required as messages are not necessarily added in timely order
  std::vector<std::pair<uu::core::Time, const Vertex*>> sorted;

  for(auto message: messages()->vertices()[0])
    {
      auto actor = get_msg_sender(message);
      auto msg_time = get_sender_time(actor);
      sorted.push_back(std::pair<uu::core::Time, const Vertex*>(msg_time, message));
    }
  
  sort(sorted.begin(), sorted.end());
  
  //Only one retwet can be made to each message
  std::vector<const uu::net::Vertex*> used;
  
  //Moving backwards from latest message to the first
  for (auto iter = sorted.rbegin(); iter != sorted.rend(); ++iter) {
    auto message = iter->second;
    auto actor = get_msg_sender(message);
    auto msg_time = get_sender_time(actor);
    auto neighbors = messages()->edges()->neighbors(message, uu::net::EdgeMode::IN);
    auto size = neighbors->size();

    ////Remove old connections
    for(auto n: neighbors[0])
      {
	auto edge = messages()->edges()->get(n, message);
	messages()->edges()->erase(edge);
      }

    //TODO Apply relative complement to reduce computational time
    //Get the messages created later than the current message
    auto selection = get_selection(sorted, msg_time);
    
    srand(time(NULL));
    for(int i = 0; i < size; i++)
      {
	//First try of selecting a random message 
	auto chosen = selection[rand() % selection.size()];

	//If that message has already been used, try again
	while(std::find(used.begin(), used.end(), chosen) != used.end()) 
	  chosen = selection[rand() % selection.size()];

	//Add the message as used
	used.push_back(chosen);
	//Add the new edge to the xnet
	messages()->edges()->add(chosen, message);

      }
  }
}

//recursively merge all messages to a certain level
std::vector<const Vertex*>
TemporalTextNetwork::
group(
      const Vertex* message,
      unsigned int level
)
{

  std::vector<const Vertex*> n_ref;
  std::vector<const Vertex*> ref;
  
  auto neighbors = messages()->edges()->neighbors(message, uu::net::EdgeMode::IN);
  if(neighbors->size() == 0)
    return ref;

  for(auto n: neighbors[0])
    {
      if(level != 0)
	{
	  auto n_neighbors = messages()->edges()->neighbors(n, uu::net::EdgeMode::IN);
	  //Ensure that merging is correct
	  n_ref = group(n, ( level - 1 ));
	  ref.insert(ref.begin(), n_ref.begin(), n_ref.end());
	}
      ref.push_back(n);
    }    
  return ref;
}



} 
}

