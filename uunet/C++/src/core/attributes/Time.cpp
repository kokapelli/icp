#include "core/attributes/Time.h"
#include "core/attributes/conversion.h"
#include "core/attributes/Value.h"


namespace uu {
namespace core {


std::ostream&
operator<<(
    std::ostream& os,
    const Time& t
)
{
    os << to_string(t, kDEFAULT_TIME_FORMAT);
    return os;
}

//Change to operator whenever knowledge is there
//Did not work as one 8/10/18
Time
time_add(
       const Time& t1,
       const Time& t2
)
{
  auto t1_e = time_to_epoch(t1);
  auto t2_e = time_to_epoch(t2);
  auto sum = t1_e + t2_e;
  
  return epoch_to_time(sum);
  
}

  //Change to operator whenever knowledge is there
//Did not work as one 8/10/18
Time
time_sub(
       const Time& t1,
       const Time& t2
)
{
  auto t1_e = time_to_epoch(t1);
  auto t2_e = time_to_epoch(t2);
  auto sum = t1_e - t2_e;
  
  return epoch_to_time(sum);
  
}

//Change to operator whenever knowledge is there
//Did not work as one 8/10/18
Time
time_mult(
       const Time& t,
       unsigned int factor
)
{
  auto t_e = time_to_epoch(t);
  auto product = t_e * factor;
  
  return epoch_to_time(product);
  
}

  //Change to operator whenever knowledge is there
//Did not work as one 8/10/18
Time
time_div(
       const Time& t,
       unsigned int divisor
)
{
  auto t_e = time_to_epoch(t);
  auto quotient = t_e / divisor;
  
  return epoch_to_time(quotient);
  
}


    //Change to operator whenever knowledge is there
//Did not work as one 8/10/18
bool
time_lt(
       const Time& t1,
       const Time& t2
)
{
  auto t1_e = time_to_epoch(t1);
  auto t2_e = time_to_epoch(t2);
  auto lt = t1_e < t2_e;
  
  return lt;
  
}

//Change to operator whenever knowledge is there
//Did not work as one 8/10/18
bool
time_leq(
       const Time& t1,
       const Time& t2
)
{
  auto t1_e = time_to_epoch(t1);
  auto t2_e = time_to_epoch(t2);
  auto leq = t1_e <= t2_e;
  
  return leq;
  
}
  
}
}

