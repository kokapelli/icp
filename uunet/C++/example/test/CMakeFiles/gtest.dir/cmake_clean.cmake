file(REMOVE_RECURSE
  "CMakeFiles/gtest"
  "CMakeFiles/gtest-complete"
  "../../ext/gtest/src/gtest-stamp/gtest-install"
  "../../ext/gtest/src/gtest-stamp/gtest-mkdir"
  "../../ext/gtest/src/gtest-stamp/gtest-download"
  "../../ext/gtest/src/gtest-stamp/gtest-update"
  "../../ext/gtest/src/gtest-stamp/gtest-patch"
  "../../ext/gtest/src/gtest-stamp/gtest-configure"
  "../../ext/gtest/src/gtest-stamp/gtest-build"
  "../../ext/gtest/src/gtest-stamp/gtest-test"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/gtest.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
