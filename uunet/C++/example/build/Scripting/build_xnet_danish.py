#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import codecs
import datetime
import json
import re
import sys

import networkx as nx

from utils import twitter_time_to_ts, twitter_time_to_ts2



    #Get unique party names and add as keys in dictionary
   # "Alternativet"
   #"Udenfor partierne"
   #"Radikale Venstre"
   #"Dansk Folkeparti"
   #"Socialistisk Folkeparti"
   #"Liberal Alliance"
   #"Venstre"
   #"Socialdemokratiet"
   #"Enhedslisten"
   #"Det Konservative Folkeparti"
   #"KristenDemokraterne"        

CURR_PARTY = 'KristenDemokraterne'
INPUT_FILE = '/home/hampus/uunet/C++/example/build/CSVs/valg2015.csv'
IDS_FILE   = '/home/hampus/uunet/C++/example/build/CSVs/valg2015ids.csv'
#INPUT_FILE = '../raw-data/interactions_data_sample.txt'
OUTPUT_FILE = '/home/hampus/uunet/C++/example/build/XNET/' + CURR_PARTY + '.xnet'
#OUTPUT_FILE = '../tiddy-data/interactions_sample.xnet'

class UIDGEnerator:
    def __init__(self):
        self.last_id = 0

    def next(self):
        self.last_id = self.last_id + 1
        return self.last_id

def main(argv):
    g = UIDGEnerator()

    ids = dict()

    actors = dict()
    messages = dict()
    hashtags = list()
    edges = list()
    intra_edges = list()

    messages_dict = dict()

    with codecs.open(IDS_FILE, 'r', encoding='utf-8', errors='replace') as input_file:
        for s in input_file.readlines():
            if len(s) > 1:
                info = s.split(";")

                actor_id = info[9].strip("\r\n")
                actor_username = info[8]
                actor_party = info[1]
                ids[actor_id] = actor_username
                ids[actor_username] = actor_party
    
        with open(INPUT_FILE, 'r') as input_file:
            for s in input_file.readlines()[1:]:
                if len(s) > 1:
                    info = s.split(",")

                    p_belong = 0
                    t_belong = 0
                    from_id = info[3]

                    if from_id not in actors:
                        actors[from_id] = g.next()
                            
                    msg = {
                        'id': info[1],
                        'uid': g.next(),
                        'timestamp': info[1],
                        'timestamp': twitter_time_to_ts2(info[2]),
                        #'timestamp': datetime.datetime.fromtimestamp(int(info[2].replace('"', ''))),
                        'text': str(info[4][1:-1])
                    }
                    
                    msg_id = msg.get('id')
                    messages[msg_id] = msg
                    
                    rt_message = "RT @" + str(from_id) + ": " + msg.get('text')
                    if rt_message not in messages_dict:
                        messages_dict[rt_message] = msg.get('id')
                        #print(rt_message)
                        
                    pattern = re.compile(r'(?<=^|(?<=[^a-zA-Z0-9-_\.]))@([A-Za-z]+[A-Za-z0-9]+)')
                    user_mentions = re.findall(pattern, msg.get('text'))
                    
                    
                        
                    for to_actor in user_mentions:
                        to_id = to_actor.replace('@', '')
                        
                        if to_id not in actors:
                            actors[to_id] = g.next()

                        if from_id in ids:
                            p_belong = ids[from_id]

                        if to_id in ids:
                            t_belong = ids[to_id]
                            
                        if (p_belong != CURR_PARTY and t_belong != CURR_PARTY):
                            break
                        
                        edges.append( ("MA", msg_id, to_id) )
                            
                        if msg.get('text').startswith('RT'):
                            if msg.get('text') in messages_dict:
                                orig_msg = messages.get( messages_dict[ msg.get('text') ] )
                                orig_msg_id = orig_msg.get('id')
                                intra_edges.append( (msg_id, orig_msg_id) )

                    edges.append( ("AM", from_id, msg_id) )

            
    #
    # PRINTING RESULTS
    #
    with open(OUTPUT_FILE, 'w') as output_file:
        output_file.write("#VERSION\n")
        output_file.write("2.0\n")

        output_file.write("\n")

        output_file.write("#INTRALAYER VERTICES\n")
        for k,v in actors.items():
            output_file.write(str(v) + "|" + "A\n")

        for k,v in messages.items():
            uid = str(v.get('uid'))
            text = str(v.get('text')).replace('\n', ' ').replace('\r', '')

            output_file.write(uid + "|M|" + text + "\n")

        output_file.write("\n")

        output_file.write("#INTERLAYER EDGES\n")
        for e in edges:
            if e[0] == "AM":
                actor_from = str(actors.get( e[1] ))
                msg = messages.get( e[2] )
                msg_id = str(msg.get('uid'))
                timestamp = str(msg.get('timestamp'))

                output_file.write(actor_from + "|A|" + msg_id + "|M|" + timestamp + "\n")

            else:
                msg = messages.get( e[1] )
                msg_id = str(msg.get('uid'))
                actor_to = str(actors.get( e[2] ))
                timestamp = str(msg.get('timestamp'))

                output_file.write(msg_id + "|M|" + actor_to + "|A|" + timestamp + "\n")

        output_file.write("\n")

        output_file.write("#INTRALAYER EDGES\n")
        for edge in intra_edges:
            from_msg = messages.get( edge[0] )
            from_id = str(from_msg.get('uid'))
            to_msg = messages.get( edge[1] )
            to_id = str(to_msg.get('uid'))

            output_file.write(from_id + "|" + to_id + "|M\n")

if __name__ == '__main__':
    main(sys.argv)
