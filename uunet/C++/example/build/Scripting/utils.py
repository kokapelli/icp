#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import time

def twitter_time_to_ts(input_time):
    twitter_time = time.strptime(input_time,'%a %b %d %H:%M:%S +0000 %Y')

    return str(time.mktime(twitter_time))[:-2]

def twitter_time_to_ts2(input_time):
    twitter_time = time.strptime(input_time.replace('"', ''),'%Y-%m-%d %H:%M:%S')

    return str(time.mktime(twitter_time))[:-2]
