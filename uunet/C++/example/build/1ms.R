library(tidyverse)
library(lubridate)
library(dplyr)
require(ggplot2)
require(scales)

normalize <- function(x) {
  return ((x - min(x)) / (max(x) - min(x)))
}

test <- read_csv(file="/home/hampus/uunet/C++/example/build/sparse_nSynch.csv")
#test$t <- as.POSIXct(test$t, format="%Y-%m-%d %H:%M:%S", tz = "GMT")
summary(test)
test

summary(test)


ggplot(data = test, aes(x = as.POSIXct(test$t), y = moving_avg))+
  geom_line() +
  scale_y_log10()

data_wide <- spread(test, t, moving_avg)
data_wide[is.na(data_wide)] <- 0
data_wide




sums <- colSums(data_wide[2:6168])
sums
n_test <- normalize(test$moving_avg)
plot(n_test)
n_sums <- normalize(sums)
plot(n_sums,type="l")

str(names(n_sums))
str(unname(n_sums))
xx <- names(n_sums)
yy <- unname(n_sums)

DTW <- data.frame(date = as.POSIXct(xx), value = yy)
str(DTW)
plot(DTW)

ww <- subset(test, msg > 8 & msg < 1000)
yy <- subset(test, msg > 1001 & msg < 10000)

plot.ts(y = ww$moving_avg, x = as.POSIXct(ww$t), type = "l")
plot.ts(y = yy$moving_avg, x = as.POSIXct(yy$t), type = "l")

ggplot() +
  geom_line(data = ww, mapping = 
              aes(x = as.POSIXct(t), y = moving_avg, group = msg), color = "blue", alpha = 4/10) +
  geom_line(data = yy, mapping = 
              aes(x = as.POSIXct(t), y = moving_avg, group = msg), color = "red", alpha = 4/10) +
  scale_y_log10() +
  scale_x_datetime(
    limits = c(as.POSIXct("2009-05-12 06:43:00 GMT"),
               as.POSIXct("2009-05-12 20:43:59 GMT")))







ggplot(data = DTW, mapping = 
         aes(x = date, y = value)) +
  geom_smooth(color='red', size=1) +
  geom_line(data = test, mapping =
              aes(x = as.POSIXct(t), 
                  y = normalize(moving_avg), 
                  group = msg), alpha = 2/10) +
  scale_y_log10() 

ggplot(data = test, mapping = 
         aes(x = as.POSIXct(t), y = normalize(moving_avg), group = msg)) +
  geom_line(aes(x = as.POSIXct(names(n_sums)), y = unname(n_sums))) +
  geom_line(alpha = 3/10) + 
  scale_y_log10() +
  scale_x_datetime(
    limits = c(as.POSIXct("2009-05-12 06:43:00 GMT"),
               as.POSIXct("2009-05-12 10:43:59 GMT")))

#labels = date_format("%H:%M"),
#xlim(as.Date(c('2009-05-12', '2009-05-14'), format="%Y-%m-%d") )
#data_wide <- spread(test, t, moving_avg)
#data_wide
#alpha = 2/10,
#subbed <- subset(test, moving_avg > 0.0)
#subbed

#stat_summary(fun.y=mean, 
#             colour="blue", 
#             geom="line", 
#             size = 3) +