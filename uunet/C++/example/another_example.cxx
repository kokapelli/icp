#include "tnet/io/read_temporal_network.h"
#include "tnet/transformation/to_ordered_multiplex.h"
#include "mnet/community/glouvain.h"

//#include "/usr/local/include/libuunet/tnet/io/read_temporal_network.h"
//#include "/usr/local/include/libuunet/tnet/transformation/to_ordered_multiplex.h"
//#include "/usr/local/include/libuunet/mnet/community/glouvain.h"

int
main()
{
    std::unique_ptr<uu::net::TemporalNetwork> temp_net;
    temp_net = uu::net::read_temporal_network("test.tnt", "test", ',');

    auto sliced_net = uu::net::to_ordered_multiplex(temp_net.get(), 3);

    auto communities = uu::net::generalized_louvain<uu::net::OrderedMultiplexNetwork, uu::net::SimpleGraph>(sliced_net.get(), 1, 1, 100);

    auto i = 0;
    
    for (auto com: *communities)
    {
        std::cout << "COMMUNITY" << i << "--------" << std::endl;
        i += i;

        for (auto vert: *com)
        {
            std::cout << vert.first->name << "@" << vert.second->name << std::endl;
        }
    }

    return 0;
}

