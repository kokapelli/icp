var dir_2fa1ed3146d6c2b96c5cfe4341985326 =
[
    [ "algorithms", "dir_a328bd242c86368002534bddd0f46650.html", "dir_a328bd242c86368002534bddd0f46650" ],
    [ "community", "dir_a563a8dfd8fde0aa763ef1fd38b3385a.html", "dir_a563a8dfd8fde0aa763ef1fd38b3385a" ],
    [ "creation", "dir_87afc2ee726006466e8b8d0a04e1eea8.html", "dir_87afc2ee726006466e8b8d0a04e1eea8" ],
    [ "datastructures", "dir_915c5dfdd2a19c3ee083f53362cc1d1e.html", "dir_915c5dfdd2a19c3ee083f53362cc1d1e" ],
    [ "io", "dir_5cb75491ec442980585fc900b9b7b59a.html", "dir_5cb75491ec442980585fc900b9b7b59a" ],
    [ "measures", "dir_1b675c230077a85d691d72aa606a94c3.html", "dir_1b675c230077a85d691d72aa606a94c3" ],
    [ "operations", "dir_54113ee0c7cea9003f63dbe564c533e2.html", "dir_54113ee0c7cea9003f63dbe564c533e2" ]
];