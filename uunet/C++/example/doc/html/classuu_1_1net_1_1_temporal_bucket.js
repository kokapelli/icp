var classuu_1_1net_1_1_temporal_bucket =
[
    [ "get_bucket_peak", "classuu_1_1net_1_1_temporal_bucket.html#a15671846a5ae07bc02492b7921f092bb", null ],
    [ "get_bucket_velocity", "classuu_1_1net_1_1_temporal_bucket.html#a0ea0e4c42e420bfd7fba05eaad170e97", null ],
    [ "get_bulk_bucket_velocity", "classuu_1_1net_1_1_temporal_bucket.html#a85764b41241536b985fc8289abe78897", null ],
    [ "get_duration", "classuu_1_1net_1_1_temporal_bucket.html#a0741e438e90376ef9c693290d848488b", null ],
    [ "get_persistence", "classuu_1_1net_1_1_temporal_bucket.html#a5dd1d5038616d0c929104aec4126a423", null ],
    [ "get_time", "classuu_1_1net_1_1_temporal_bucket.html#adbfa7c6c3342ac1a37e285a723d3ddfc", null ],
    [ "get_vertex", "classuu_1_1net_1_1_temporal_bucket.html#a86f0acaa512e0e44ed7874bf106c43c2", null ],
    [ "synch_referrers", "classuu_1_1net_1_1_temporal_bucket.html#ae9499e481c4968c7b96b006ae9bd8c71", null ],
    [ "interval", "classuu_1_1net_1_1_temporal_bucket.html#a779f5246698b4cf7c76891e6da8b8459", null ],
    [ "min_time", "classuu_1_1net_1_1_temporal_bucket.html#a4cb7b8b3f7368d296d1f80c85481af9e", null ],
    [ "referrers", "classuu_1_1net_1_1_temporal_bucket.html#afb94c3c4f48ca3ed6de9f160fc7b8664", null ],
    [ "t", "classuu_1_1net_1_1_temporal_bucket.html#a61209763dc524930ef8bf7875d6a9639", null ]
];