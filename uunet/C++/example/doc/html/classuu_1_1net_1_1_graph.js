var classuu_1_1net_1_1_graph =
[
    [ "Graph", "classuu_1_1net_1_1_graph.html#a04bd55b83a27e6e5a9663ce18b9b4fb5", null ],
    [ "allows_loops", "classuu_1_1net_1_1_graph.html#a43a562821981bf483088660971c80c7b", null ],
    [ "allows_multi_edges", "classuu_1_1net_1_1_graph.html#a822c779b7446672ab49f87b9697e724d", null ],
    [ "edges", "classuu_1_1net_1_1_graph.html#a01cfff32aee25f226f3611771677a564", null ],
    [ "edges", "classuu_1_1net_1_1_graph.html#a0c4f1c55f1af47f6ad61f7ff757d72ae", null ],
    [ "is_attributed", "classuu_1_1net_1_1_graph.html#a2070929aecbf2ad68e917279cc9cd53f", null ],
    [ "is_directed", "classuu_1_1net_1_1_graph.html#a6ff48c07693099523bd1104e278ceced", null ],
    [ "is_probabilistic", "classuu_1_1net_1_1_graph.html#af430e671742de88cce31a57a4892719e", null ],
    [ "is_temporal", "classuu_1_1net_1_1_graph.html#a5c00d0aa0216317f76ac881e8c14d5a3", null ],
    [ "is_weighted", "classuu_1_1net_1_1_graph.html#a20c2f86b96532cf0af0afc3d61d82a03", null ],
    [ "summary", "classuu_1_1net_1_1_graph.html#ad45191180d1232e97ffbee697806958c", null ],
    [ "vertices", "classuu_1_1net_1_1_graph.html#acaf6280ac5cf05592168bacf47de7f72", null ],
    [ "vertices", "classuu_1_1net_1_1_graph.html#a855098668dafb0527c7974a30306ca0f", null ],
    [ "edges_", "classuu_1_1net_1_1_graph.html#a704b621a2626f723ebe079f1155b2a39", null ],
    [ "name", "classuu_1_1net_1_1_graph.html#a0cb988d513efcacba9def8ebf256042b", null ],
    [ "type_", "classuu_1_1net_1_1_graph.html#a85764621a7db70cb23d8feae7bcdc1a8", null ],
    [ "vertices_", "classuu_1_1net_1_1_graph.html#a64f28c18c05132f8bd95c1c5a9a803c1", null ]
];