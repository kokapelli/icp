var classuu_1_1net_1_1_simple_edge_store =
[
    [ "SimpleEdgeStore", "classuu_1_1net_1_1_simple_edge_store.html#a841e52c1ffbd1ce823bfdd1736f28b72", null ],
    [ "add", "classuu_1_1net_1_1_simple_edge_store.html#ac0f9bf5c1e50e444d6273157dd0a6bd5", null ],
    [ "add", "classuu_1_1net_1_1_simple_edge_store.html#a5bb632614e2ab2df400711c7ac431ed2", null ],
    [ "erase", "classuu_1_1net_1_1_simple_edge_store.html#a9245fbf2ed9d280ab7c41e54b7a0b8e8", null ],
    [ "erase", "classuu_1_1net_1_1_simple_edge_store.html#ad2be1a00dff58474c565ed97e8906031", null ],
    [ "get", "classuu_1_1net_1_1_simple_edge_store.html#afa5464a37e6e397eb1049d313022299b", null ],
    [ "summary", "classuu_1_1net_1_1_simple_edge_store.html#a15fdc59ad7640c9d612c120fa65b43c5", null ],
    [ "cidx_edge_by_vertexes", "classuu_1_1net_1_1_simple_edge_store.html#aa267c980ac24aa20ba971de6c0e5c715", null ]
];