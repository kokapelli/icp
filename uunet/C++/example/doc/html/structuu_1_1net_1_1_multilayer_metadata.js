var structuu_1_1net_1_1_multilayer_metadata =
[
    [ "edge_attributes", "structuu_1_1net_1_1_multilayer_metadata.html#a2c58e11173cea664cd2fabca6184e1d7", null ],
    [ "features", "structuu_1_1net_1_1_multilayer_metadata.html#af481b1d09fa31e4efbce4fa13d8e72ab", null ],
    [ "intralayer_edge_attributes", "structuu_1_1net_1_1_multilayer_metadata.html#a40460c0d9eb68cc0c5268314ade1c24a", null ],
    [ "intralayer_vertex_attributes", "structuu_1_1net_1_1_multilayer_metadata.html#a0a86ebb378b5144e24acac24e56f2abc", null ],
    [ "layers", "structuu_1_1net_1_1_multilayer_metadata.html#a02b60bd91ffb807be80d2cf2ce846741", null ],
    [ "vertex_attributes", "structuu_1_1net_1_1_multilayer_metadata.html#a22c8b45e19e1940f4fda8eaa1e5bd79e", null ]
];