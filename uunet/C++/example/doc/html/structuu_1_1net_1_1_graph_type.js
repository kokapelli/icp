var structuu_1_1net_1_1_graph_type =
[
    [ "allows_loops", "structuu_1_1net_1_1_graph_type.html#ab78092a9473d0457970f1b2241dce573", null ],
    [ "allows_multi_edges", "structuu_1_1net_1_1_graph_type.html#a9e9e469bfb1a9700a214cd96317544ad", null ],
    [ "is_attributed", "structuu_1_1net_1_1_graph_type.html#ad0b82524b272810c0650a46cfabfe1e8", null ],
    [ "is_directed", "structuu_1_1net_1_1_graph_type.html#af356ade99b85e0eef2a0b1e1d102fb5c", null ],
    [ "is_probabilistic", "structuu_1_1net_1_1_graph_type.html#a5d8a12f24f9471e456b6b857891b59cf", null ],
    [ "is_temporal", "structuu_1_1net_1_1_graph_type.html#a3ad6399a016fd85cc88faa192f54b1ab", null ],
    [ "is_weighted", "structuu_1_1net_1_1_graph_type.html#a9786eb39acec7c453128ae48362cb49a", null ]
];