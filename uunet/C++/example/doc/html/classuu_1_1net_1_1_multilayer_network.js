var classuu_1_1net_1_1_multilayer_network =
[
    [ "MultilayerNetwork", "classuu_1_1net_1_1_multilayer_network.html#af8339b43ad3a1bd3770901415271a586", null ],
    [ "interlayer_edges", "classuu_1_1net_1_1_multilayer_network.html#a36ecbf80d7e95b2677e8ca6267dd5bcd", null ],
    [ "interlayer_edges", "classuu_1_1net_1_1_multilayer_network.html#a8398c785e0859a202d49bb278d37aca8", null ],
    [ "is_ordered", "classuu_1_1net_1_1_multilayer_network.html#a19c8ddbce32a79abfd56d90dfc0e90d6", null ],
    [ "layers", "classuu_1_1net_1_1_multilayer_network.html#a77bb697062ec04772e48604577e6b754", null ],
    [ "layers", "classuu_1_1net_1_1_multilayer_network.html#a1137567420a138e4f6126a2932af9af4", null ],
    [ "summary", "classuu_1_1net_1_1_multilayer_network.html#af531b3be80a75f7a72b36e66dbb157cc", null ],
    [ "vertices", "classuu_1_1net_1_1_multilayer_network.html#af0e7221fadbf0e5e0e467386c99d9a5e", null ],
    [ "vertices", "classuu_1_1net_1_1_multilayer_network.html#a178746a584365cc5e39cd0decee39664", null ],
    [ "edges_", "classuu_1_1net_1_1_multilayer_network.html#a698d3883cea382d179dc50c5d7273300", null ],
    [ "layers_", "classuu_1_1net_1_1_multilayer_network.html#a09da028f922873db4c4c158d587e40a6", null ],
    [ "name", "classuu_1_1net_1_1_multilayer_network.html#a601762fab69d12cfa90722c29462825d", null ],
    [ "obs_", "classuu_1_1net_1_1_multilayer_network.html#a87ab635e11fef4d4781ac4d3b8910e4f", null ],
    [ "type_", "classuu_1_1net_1_1_multilayer_network.html#a37bab60aeeeea2347f93dc63f9efc420", null ],
    [ "vertices_", "classuu_1_1net_1_1_multilayer_network.html#adc4c8a549601d8bb29011832470bad57", null ]
];