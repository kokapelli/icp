#include "xnet/datastructures/graphs/TemporalTextNetwork.h"
#include "xnet/io/read_temporal_text_network.h"
#include "core/attributes/conversion.h"
#include "xnet/measures/moving_avg.h"
#include "xnet/measures/degree.h"

#include <utility>
#include <fstream>

int main(int argc, char **argv)
{

  //interactions_v3.xnet
  //enron_rand_300.xnet
  clock_t tStart = clock();
  std::unique_ptr<uu::net::TemporalTextNetwork> xnet;
  //XNET/Parties/KristenDemokraterne.xnet
  xnet = uu::net::read_temporal_text_network("XNET/demo2.tnt", "test", '|');
  //xnet->randomize_intralayer_IN();
  std::cout << "xnet Created..." << std::endl;
  printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
  
  //Day 86400
  //hour 3600
  uu::core::Time interval =  uu::core::epoch_to_time(60);
  unsigned int window = 3;
  unsigned int iterations = 10;

  //auto xmas = xnet->moving_average<const uu::net::Vertex*>(interval, window, 1);
  //std::cout <<  "xmas Created..." << std::endl;
  //printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
  auto tbs = xnet->create_tbs<const uu::net::Vertex*>(interval);
  // auto tbs = xnet->create_grouped_tbs<const uu::net::Vertex*>(interval, 2);
  std::cout <<  "tbs Created..." << std::endl;
  printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

      
  std::ofstream myFile1;
  myFile1.open("1_min_3_inter_random_spec.csv");

  myFile1 <<
    "i" <<
    "," << 
    "msg" <<
    "," <<
    "actor" <<
    "," <<
    "a_out" <<
    "," <<
    "a_in" <<
    "," <<
    "response_amount" <<
    "," <<
    "peak" <<
    "," <<
    "peak_reached_after"
    "," <<
    "max_acceleration" <<
    "," <<
    "min_acceleration" <<
    "," <<
    "production_velocity" <<
    "," <<
    "response_velocity" <<
    "," <<
    "sd_persistence" <<
    std::endl;

  
  for(int i = 0; i < iterations; i++)
    {
      xnet->randomize_intralayer_IN();
      //Synched == 1
      auto xmas = tbs->tbs_moving_average(interval, window, 0);
      std::cout <<  "MA Created..." << std::endl;
      printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
      
      for(auto &ma: xmas)
	{
	  auto peak = uu::net::get_ma_peak(ma.second);
	  
	  //Check legitimacy with smaller data set
	  auto time_peak_diff = uu::core::time_sub(peak.first, ma.second.begin()->first);
	  auto actor = xnet->get_msg_sender(ma.first);
	  auto sdp = uu::core::time_to_epoch(uu::net::get_ma_sd_persistence(ma.second));
	  auto a_out = uu::net::interlayer_degree(xnet.get(), actor->v1, uu::net::EdgeMode::OUT);
	  auto a_in = uu::net::interlayer_degree(xnet.get(), actor->v1, uu::net::EdgeMode::IN);
	  auto v = uu::net::get_ma_velocity(ma.second, interval);
	  auto acc = uu::net::get_ma_velocity(v, interval);
	  auto v_max = uu::net::get_ma_max_velocity(v);
	  auto v_min = uu::net::get_ma_min_velocity(v);
	  auto sum = uu::net::get_ma_sum(ma.second);
	  auto vb1 = tbs->get_bucket(ma.first).get_bucket_velocity(1); //production
	  auto vb0 = tbs->get_bucket(ma.first).get_bucket_velocity(0);
	  myFile1  <<
	    i <<
	    "," << 
	    *ma.first  <<
	    "," <<
	    *actor->v1 <<
	    ","  <<
	    a_out <<
	    "," <<
	    a_in <<
	    "," <<
	    sum <<
	    "," <<
	    peak.second <<
	    "," <<
	    uu::core::time_to_epoch(time_peak_diff) <<
	    "," <<
	    v_max.second <<
	    "," <<
	    v_min.second <<
	    "," <<
	    vb1 <<
	    "," <<
	    vb0 <<
	    "," <<
	    uu::core::to_string(sdp)  <<
	    std::endl;
	}
      std::cout << "Iteration:" << i << " computed..." << std::endl;
      printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
    }
  std::cout << "Myfile1 computed..." << std::endl;
  printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);

  /*
  std::ofstream myFile2;
  myFile2.open("1_min_3_inter_group_2_bulk.csv");
  
  myFile2 <<
    "msg" <<
    "," <<
    "time" <<
    "," <<
    "moving_avg"  <<
    "," <<
    "acceleration" << std::endl;
  for(auto &ma: xmas)
    {
      //auto bucket = tbs->get_bucket(ma.first);
      //auto ref = bucket.referrers;
      auto v2 = uu::net::get_ma_velocity(ma.second, interval);
      auto acc = uu::net::get_ma_velocity(v2, interval);
      for(auto &m: ma.second)
	{
	  myFile2 << *ma.first <<
	    "," <<
	    uu::core::to_string(m.first) <<
	    "," << 
	    m.second <<
	    "," <<
	    acc.find(m.first)->second <<
	    std::endl;
	}
      myFile2 << std::endl;
    }      
  
  //"responses" <<
  //","
  //"," <<
  //ref.count(m.first) * ref.find(m.first)->second <<
  
  std::cout << "Myfile2 computed..." << std::endl;
  printf("Time taken: %.2fs\n", (double)(clock() - tStart)/CLOCKS_PER_SEC);
  */

  return 0;
}
